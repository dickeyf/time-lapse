import json
import os
import paho.mqtt.client as mqtt
from datetime import datetime
import cv2
import numpy as np
from flask import Flask, send_file, Response
from pathlib import Path
from flask_cors import CORS
import re

# defaults to 1 hour backlog (At 5 frames/second)
buffer_limit = 1000000
time_lapse_buffers = {}
store_location = "./test-store/"
app = Flask(__name__)
cors = CORS(app, resources={r"/videos/*": {"origins": ["http://localhost:3000", "https://*.dickey.cloud/*"]}})


def parse_info_from_filename(filename):
    m = re.search('(\d{4})-(\d{2})-(\d{2})_(\d{2}):(\d{2}):(\d{2})_(\d+)seconds.mp4', filename)

    if m:
        year = int(m.group(1))
        month = int(m.group(2))
        day = int(m.group(3))
        hour = int(m.group(4))
        minute = int(m.group(5))
        seconds = int(m.group(6))
        duration = int(m.group(7))
        video_date = datetime(year,month,day,hour,minute,seconds)
        video_info = {
            "date": int(video_date.timestamp()),
            "duration": duration,
            "filename": filename
        }
        return video_info
    else:
        video_info = {
            "filename": filename,
            "date": 0
        }
        return video_info


def get_stream_path(datacenter_id, device_id):
    stream_path = os.path.join(store_location, datacenter_id, device_id)
    stream_mp4_path = os.path.join(store_location, datacenter_id, device_id, "mp4")
    Path(stream_mp4_path).mkdir(parents=True, exist_ok=True)
    return stream_path, stream_mp4_path


@app.route('/videos/<datacenter_id>/<device_id>')
def get_video_list(datacenter_id, device_id):
    stream_location, _ = get_stream_path(datacenter_id, device_id)
    files = [parse_info_from_filename(f) for f in os.listdir(stream_location) if os.path.isfile(os.path.join(stream_location, f))]
    files.sort(key=lambda x: -x['date'])
    data = {
        "data": files
    }
    js = json.dumps(data)
    resp = Response(js, status=200, mimetype='application/json')
    return resp


@app.route('/videos/<datacenter_id>/<device_id>/<path:filename>')
def get_video(datacenter_id, device_id, filename):
    stream_location, _ = get_stream_path(datacenter_id, device_id)
    return send_file(stream_location + "/" + filename, as_attachment=True)


def insert_picture(stream_id, picture, timestamp):
    if stream_id not in time_lapse_buffers:
        time_lapse_buffers[stream_id] = []
    time_lapse_buffer = time_lapse_buffers[stream_id]
    time_lapse_buffer.append({
        "picture": picture,
        "seconds": timestamp})
    # Trim the buffer FIFO way when we exceed our limit
    if len(time_lapse_buffer) > buffer_limit:
        time_lapse_buffer.pop(0)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribe to previews feed, this is what we store
    client.subscribe("iot/sensor/+/+/camera/frames")

    # Subscribe to motion detection events, this is what makes us dump a time-lapse mjpeg
    client.subscribe("iot/sensor/+/+/camera/motion")


def get_timelapse(stream_id, startTimeSecs, endTimeSecs):
    if stream_id not in time_lapse_buffers:
        return []
    time_lapse_buffer = time_lapse_buffers[stream_id]
    timelapse = []
    for frame in time_lapse_buffer:
        if startTimeSecs <= frame["seconds"] <= endTimeSecs:
            timelapse.append(frame["picture"])
    return timelapse


def get_opencv_img_from_buffer(buffer, flags):
    bytes_as_np_array = np.frombuffer(buffer, dtype=np.uint8)
    return cv2.imdecode(bytes_as_np_array, flags)


def read_timestamp(raw_timestamp):
    return raw_timestamp[0] + raw_timestamp[1]*256 + raw_timestamp[2]*256*256 + raw_timestamp[3]*256*256*256


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    topic = msg.topic
    datacenter_id = topic.split("/")[2]
    device_id = topic.split("/")[3]
    stream_id = datacenter_id+"/"+device_id

    # Handle preview streams here
    if topic.endswith("camera/frames"):
        payload = msg.payload
        raw_timestamp = payload[:4]
        frame_timestamp = read_timestamp(raw_timestamp)
        jpeg = payload[4:]
        insert_picture(stream_id, jpeg, frame_timestamp)

    # Handle motion detected events here
    if topic.endswith("camera/motion"):
        json_payload = msg.payload
        payload = json.loads(json_payload)
        start_time = payload["begin_timestamp"]
        end_time = payload["end_timestamp"]

        time_lapse = get_timelapse(stream_id, start_time, end_time)
        if len(time_lapse) > 0:
            duration = (1 + end_time - start_time)
            frame_rate = len(time_lapse) / duration
            codec = cv2.VideoWriter_fourcc(*'mp4v')
            timestring = datetime.utcfromtimestamp(start_time).strftime('%Y-%m-%d_%H:%M:%S')
            filename = timestring + "_" + str(duration) + "seconds.mp4"
            stream_location, stream_mp4_location = get_stream_path(datacenter_id, device_id)
            mp4_path = stream_mp4_location + "/" + filename
            h264_path = stream_location + "/" + filename
            writer = cv2.VideoWriter(mp4_path, codec,
                                     frame_rate, (640, 480))
            for frame in time_lapse:
                writer.write(get_opencv_img_from_buffer(frame, cv2.IMREAD_COLOR))
            writer.release()

            os.system(f"ffmpeg -y -i {mp4_path} -vcodec libx264 {h264_path}")

            new_video_event = {
                "begin_timestamp": payload["begin_timestamp"],
                "end_timestamp": payload["end_timestamp"],
                "filename": filename
            }
            client.publish("iot/sensor/" + datacenter_id + "/" + device_id + "/camera/video",
                           json.dumps(new_video_event))


# Specifying the frame limit is optional
if "FRAME_LIMIT" in os.environ:
    buffer_limit = int(os.environ["FRAME_LIMIT"])


if "STORE" in os.environ:
    store_location = os.environ["STORE"]


# Collect the Solace PubSub+ connection parameters from the ENV vars
vmr_host = os.environ["VMR_HOST"]
mqtt_port = os.environ["MQTT_PORT"]
mqtt_username = os.environ["MQTT_USERNAME"]
mqtt_password = os.environ["MQTT_PASSWORD"]

# Establish connection with the Solace PubSub+ broker
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set(mqtt_username, mqtt_password)
client.connect(vmr_host, int(mqtt_port), 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_start()

app.run(host='0.0.0.0')
